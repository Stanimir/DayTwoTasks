package main;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Main {

	public static void main(String[] args) throws IOException {
		TreeMap<String, Integer> map = new TreeMap<>();
		Scanner scaner = new Scanner(new File("resources/text"));
		String[] words = { "" };
		String text = scaner.useDelimiter("\\Z").next();
		scaner.close();
		text = text.toLowerCase();

		Pattern pt = Pattern.compile("[^a-zA-Z ]");
		Matcher match = pt.matcher(text);
		
		while (match.find()) {
			String s = match.group();
			text = text.replaceAll("\\" + s, "");
		}

		words = text.split(" ");

		for (String word : words) {

			if (map.containsKey(word)) {
				map.put(word, map.get(word) + 1);
			} else {
				map.put(word, 1);
			}

		}

		System.out.println(map);
	}

}
