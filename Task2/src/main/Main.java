package main;

import java.util.Map.Entry;
import java.util.ArrayList;
import java.util.Random;
import java.util.concurrent.ConcurrentSkipListMap;

public class Main {
	static ConcurrentSkipListMap<CustomKey, Integer> map = new ConcurrentSkipListMap<>();

	private static void fillMap(ConcurrentSkipListMap<CustomKey, Integer> map) {
		Random random = new Random();
		CustomKey key = new CustomKey(random.nextInt(Integer.MAX_VALUE), random.nextBoolean(), random.nextBoolean());
		
		while (map.size() < 1_000_000) {
			map.put(key, random.nextInt(Integer.MAX_VALUE));
			key.setValue(random.nextInt(Integer.MAX_VALUE));
			key.setSecondValue(random.nextBoolean());
			key.setThirdValue(random.nextBoolean());
		}

	}

	private static void printEven(ConcurrentSkipListMap<CustomKey, Integer> map) {
		ArrayList<Integer> values = new ArrayList<>();

		for (Entry<CustomKey, Integer> entry : map.entrySet()) {
			if (entry.getValue() % 2 == 0) {
				values.add(entry.getValue());
			}
		}
		System.out.println(values);
	}

	private static void printOdd(ConcurrentSkipListMap<CustomKey, Integer> map) {
		ArrayList<Integer> values = new ArrayList<>();

		for (Entry<CustomKey, Integer> entry : map.entrySet()) {
			if (entry.getKey().getValue() % 2 == 0) {
				values.add(entry.getKey().getValue());
			}
		}
		System.out.println(values);
	}

	public static void main(String[] args) {

		Runnable task1 = () -> fillMap(map);
		new Thread(task1).start();

		while (map.size() < 1_000_000) {

			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}

		}

		Runnable task2 = () -> printEven(map);
		new Thread(task2).start();

		Runnable task3 = () -> printOdd(map);
		new Thread(task3).start();

	}

}
