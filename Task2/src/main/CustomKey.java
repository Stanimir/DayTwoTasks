package main;

public class CustomKey implements Comparable<CustomKey> {
	private int value;
	private boolean secondValue;
	private boolean thirdValue;

	public CustomKey(int value, Boolean secondValue, Boolean thirdValue) {
		super();
		this.value = value;
		this.secondValue = secondValue;
		this.thirdValue = thirdValue;
	}

	

	public int getValue() {
		return value;
	}



	public void setValue(int value) {
		this.value = value;
	}



	public boolean isSecondValue() {
		return secondValue;
	}



	public void setSecondValue(boolean secondValue) {
		this.secondValue = secondValue;
	}



	public boolean isThirdValue() {
		return thirdValue;
	}



	public void setThirdValue(boolean thirdValue) {
		this.thirdValue = thirdValue;
	}



	@Override
	public int compareTo(CustomKey o) {
		if (this.value == o.value && this.secondValue == o.secondValue && this.thirdValue == o.thirdValue)
			return -1;
		return -1;
	}

}
