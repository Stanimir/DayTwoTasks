package main;

import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.TreeMap;

public class Main {
	
	private static void fillMap(Map<Integer,Integer> map)
	{
		Random random = new Random();
		int count = 0;
		while(map.size() < 1_000_000)
		{
			map. putIfAbsent(count, random.nextInt(2_000_000));
			count++;
		}
		
	}
	
	public static void main(String[] args) {
		TreeMap<Integer, Integer> treeMap = new TreeMap<>();
		HashMap<Integer, Integer> hashMap = new HashMap<>();
		
		
		
		
		
		long start = System.currentTimeMillis();
		fillMap(hashMap);
		System.out.println("HashMap time: " + (System.currentTimeMillis() - start));
		
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		start = System.currentTimeMillis();
		fillMap(treeMap);
		System.out.println("TreeMap time: " + (System.currentTimeMillis() - start));
		
	}
}
